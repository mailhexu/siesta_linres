! 
! Copyright (C) 1996-2016	The SIESTA group
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt.
! See Docs/Contributors.txt for a list of contributors.
!
      module densematrix
C
C  Contains the dense matrix arrays used within SIESTA
C
      use precision

      implicit none

      ! Ensure they are initialially nullified
      real(dp), pointer :: Haux(:) => null()
      real(dp), pointer :: Saux(:) => null()
      real(dp), pointer :: psi(:) => null()

      CONTAINS

      subroutine resetDenseMatrix( )
      use alloc, only : de_alloc
      implicit none

      call de_alloc( Haux, 'Haux', 'densematrix' )
      call de_alloc( Saux, 'Saux', 'densematrix' )
      call de_alloc( psi,  'psi',  'densematrix' )
      nullify(Haux, Saux, psi)

      end subroutine resetDenseMatrix

      end module densematrix
